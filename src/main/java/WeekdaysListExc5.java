import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by steamant on 20.06.17.
 */

/*
* Construct a list containing all weekdays for an arbitrary period in an arbitrary year.
* No Saturdays nor Sundays are supposed to be in this list. Write a class containing the methods that you need for
* constructing such a list. Take into account that the content in this list may be presented in more than one way.
* You can assume that your functions only responsibility is to return this list
* todo: validation of input
* todo: result validation
*/

class WeekdaysListExc5 {

    private static Temporal beginDate = userDatePrompter("beginning");
    private static Temporal endDate = userDatePrompter("ending");
    private static List<String> weekdays = new ArrayList<>();

    private static Temporal userDatePrompter(String dateDescription) {

        System.out.println("Enter " + dateDescription + " date (dd/MM/yyyy): ");
        Scanner scanner = new Scanner(System.in);
        String dateFromUser = scanner.next();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        return LocalDate.parse(dateFromUser, formatter);
    }

    static void weekDaysListProvider() {

//        Long gapInDays = ChronoUnit.DAYS.between(beginDate, endDate);
//        System.out.println(DayOfWeek.from(beginDate));
//        System.out.println(DayOfWeek.from(endDate));

        Temporal cyclingDate = beginDate;

        while (!(cyclingDate.equals(endDate))) {
            if (!(DayOfWeek.from(cyclingDate).equals(DayOfWeek.SATURDAY) || DayOfWeek.from(cyclingDate).equals(DayOfWeek.SUNDAY))) {
                weekdays.add(String.valueOf(DayOfWeek.from(cyclingDate)));
            }
            cyclingDate = cyclingDate.with(TemporalAdjusters.ofDateAdjuster(beginDate -> beginDate.plusDays(1)));
        }

        if (!(DayOfWeek.from(endDate).equals(DayOfWeek.SATURDAY) || DayOfWeek.from(endDate).equals(DayOfWeek.SUNDAY))) {
            weekdays.add(String.valueOf(DayOfWeek.from(endDate)));
        }

        System.out.println(weekdays.toString());
//        System.out.println(weekdays.size());
    }
}

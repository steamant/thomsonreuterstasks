import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by steamant on 03.06.17.
 */

// I did not followed two of the rules, one stated in the task description, other for just make output more tidy.
// (1) The expected output is 'Actual: yyyy-MM-dd:', mine is 'Actual: yyyy-MMM-dd:'; it was just a matter of another
// Strings play I have consciously omitted. (2) Other is sorting of data by date, I have omitted it having in mind
// it was not stated in the task description.

class EnergyCalculatorExc2 {

    private final static String SITE = "Http://www.mercado.ren.pt/EN/Electr/MarketInfo/Interconnections/CapForecast/Pages/Daily.aspx";
    private final static String CSS_QUERY = "#ctl00_ctl14_g_87d6ce45_f4aa_4e11_9bcc_05d4a41ea60d > div:nth-child(2) > div > table > tbody";

    static HashMap<String, Integer> dataParser() {

        HashMap<String, Integer> dailyData = new HashMap<>();

        try {
            Document document = Jsoup.connect(SITE).get();

            Elements table = document.select(CSS_QUERY);
            Elements rows = table.select("tr");

            for (Element row : rows) {
                Elements columns = row.select("td");
                for (int i = 0; i < columns.size(); i++) {
                    Element column = columns.get(i);
                    String nodeTextParsed = column.parent()
                            .firstElementSibling()
                            .nextElementSibling()
                            .child(i)
                            .text()
                            .replace("\u00A0", "")
                            .toUpperCase();
                    String[] nodeTextArray = nodeTextParsed.split("");
                    String nodeText;
                    if (nodeTextArray.length == 5) {
                        nodeText = nodeTextArray[2]
                                + nodeTextArray[3]
                                + nodeTextArray[4]
                                + "-"
                                + nodeTextArray[0]
                                + nodeTextArray[1];
                    } else {
                        continue;
                    }

                    if (column.hasClass("txtrVERIF")) {
                        //printActualValues(column, nodeText);
                        countActualValues(dailyData, column, nodeText);

                    } else if (column.hasClass("txtrPREV")) {
                        //printForecastedValues(column, nodeText);
                        countForecastedValues(dailyData, column, nodeText);

                    } else if (column.hasClass("txtVERIF")) {
                        //printActualValues(column, nodeText);
                        countActualValues(dailyData, column, nodeText);

                    } else if (column.hasClass("txtPREV")) {
                        //printForecastedValues(column, nodeText);
                        countForecastedValues(dailyData, column, nodeText);
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("Error at data gathering: " + e.getMessage());
            e.printStackTrace();
        }

        printDailyData(dailyData);
        return dailyData;
    }

    private static void printDailyData(HashMap<String, Integer> data) {
        for (String name : data.keySet()) {
            String value = data.get(name).toString();
            System.out.println(name + ": " + value);
        }
    }

    private static void printActualValues(Element column, String nodeText) {
        System.out.print("Actual: " + nodeText);
        System.out.print(" " + column.text());
        System.out.println();
    }

    private static void printForecastedValues(Element column, String nodeText) {
        System.out.print("Forecast: " + nodeText);
        System.out.print(" " + column.text());
        System.out.println();
    }

    private static void countActualValues(HashMap<String, Integer> dailyData, Element column, String nodeText) {
        if (dailyData.get("Actual: 2017-" + nodeText) != null) {
            Integer tempValue = dailyData.get("Actual: 2017-" + nodeText);
            tempValue += Integer.parseInt(column.text());
            dailyData.put("Actual: 2017-" + nodeText, tempValue);
        } else {
            dailyData.put("Actual: 2017-" + nodeText, Integer.parseInt(column.text()));
        }
    }

    private static void countForecastedValues(HashMap<String, Integer> dailyData, Element column, String nodeText) {
        if (dailyData.get("Forecast: 2017-" + nodeText) != null) {
            Integer tempValue = dailyData.get("Forecast: 2017-" + nodeText);
            tempValue += Integer.parseInt(column.text());
            dailyData.put("Forecast: 2017-" + nodeText, tempValue);
        } else {
            dailyData.put("Forecast: 2017-" + nodeText, Integer.parseInt(column.text()));
        }
    }
}

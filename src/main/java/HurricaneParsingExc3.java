import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by steamant on 09.06.17.
 */

/*
* Now you're interested in the historical hurricane information in Eastern North Pacific region. This information
* is provided by National Hurricane Center. Download the text file,then write a small program in Java to output
* the storm name and maximum sustained windspeed in knots for each storm in 2009 season.
* Format description: http://www.nhc.noaa.gov/data/hurdat/hurdat2-format-nencpac.pdf
* Hurricane tracks file: http://www.nhc.noaa.gov/data/hurdat/hurdat2-nepac-1949-2016-041317.txt
*/

/*
* Comment: Unlike in the exercise description, I parse file directly from source, not the downloaded one. I did it in
* order to spare memory load. Option --> as YEAR is declared final, changing it gives result from desired year. It can
* be used to give result on user input. To further optimise the code:
* todo: extracting of reading the file to relevant class providing every line as an String object to further parse.
* todo: make "work in progress" indicator or progress bar
* todo: validation of URL/URI
* todo: user interface
*
*/

class HurricaneParsingExc3 {

    private static final String YEAR = "2009";

    private static String tempHighestName;
    private static String highestName;
    private static int highestSpeed = 0;
    private static Map<String, Integer> hurricanesList = new HashMap<>();

    static void generateAnswer(String fileLocation, String fileName) throws IOException {

        String inputLine;

        URL fileUrl = new URL(fileLocation + fileName);

        BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(fileUrl.openStream()));

        while ((inputLine = bufferedReader.readLine()) != null) {

            findHighestResult(parseLine(inputLine, YEAR));

        }

        bufferedReader.close();

//        System.out.println("Storm with maximum sustained wind speed in " + YEAR + ", was: "
//                + highestName + ", " + highestSpeed + " kts");

        System.out.println("List of storms and their maximum speeds in " + YEAR + " :");
        for (Map.Entry<String, Integer> entry : hurricanesList.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue()+ " kts");
        }
    }

    private static String[] parseLine(String inputLine, String year) {

        String[] nameAndSpeed = new String[2];

        if (inputLine.contains("EP") || inputLine.contains("CP")) {
            String hurricaneName = inputLine.substring(10, 28).trim();
            nameAndSpeed[0] = hurricaneName;
        } else if (inputLine.contains(year)) {
            String hurricaneSpeed = inputLine.substring(38, 41).trim();
            nameAndSpeed[1] = hurricaneSpeed;
        }

        return nameAndSpeed;
    }

    private static void findHighestResult(String[] line) {

        if (line[0] != null) {
            tempHighestName = line[0];
        }

        if (highestName != null && !highestName.equals(tempHighestName)) {
            highestSpeed = 0;
        }

        if (line[1] != null && Integer.valueOf(line[1]) > highestSpeed) {

            highestSpeed = Integer.valueOf(line[1]);
            highestName = tempHighestName;

            hurricanesList.put(tempHighestName, highestSpeed);
        }
    }
}

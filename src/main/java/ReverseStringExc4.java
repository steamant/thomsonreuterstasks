/**
 * Created by steamant on 03.06.17.
 */

class ReverseStringExc4 {

    static String reverse(String s) {

        String afterReverse = "";

        if (s != null && !s.isEmpty()) {
            String[] arrayToRead = s.split("");
            String[] arrayToFill = new String[arrayToRead.length];
            for (int i = 0; i < arrayToRead.length; i++) {
                arrayToFill[i] = arrayToRead[arrayToRead.length - 1 - i];
            }
            afterReverse = String.join("", arrayToFill);
        } else {
            System.out.println("Wrong data entered.");
        }
        System.out.println(afterReverse);
        return afterReverse;
    }
}






import java.io.IOException;

/**
 * Created by steamant on 03.06.17.
 */
public class Runner {

    private static final String FILE_LOCATION = "http://www.nhc.noaa.gov/data/hurdat/";
    private static final String FILE_NAME = "hurdat2-nepac-1949-2016-041317.txt";
    private static final String STRING = "HelloDolly IsTheString ToBeReversed";

    public static void main (String[] arg) {

        EnergyCalculatorExc2.dataParser();
        System.out.println();

        try {
            HurricaneParsingExc3.generateAnswer(FILE_LOCATION, FILE_NAME);
        } catch (IOException e) {
            System.out.println("error: "+e.getMessage());
            e.printStackTrace();
        }
        System.out.println();


        ReverseStringExc4.reverse(STRING);
        System.out.println();

        WeekdaysListExc5.weekDaysListProvider();

    }
}
